package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import org.springframework.beans.factory.annotation.Autowired;

@SpringUI
@Theme("mytheme")
public class MyUI extends UI {

    @Autowired
    MainView mainView;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setContent(mainView);
    }
}
                                