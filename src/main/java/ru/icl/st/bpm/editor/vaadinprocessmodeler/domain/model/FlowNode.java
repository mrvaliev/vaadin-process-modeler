package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model;

import javax.persistence.*;
import java.util.*;


/**
 * Узел графа : либо действие, либо шлюз процесса.
 * Тип действия (заранее заданный) указывается в соответствующем свойстве
 */
@Entity
public class FlowNode extends FlowElement {


    String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @ManyToOne(cascade = {CascadeType.PERSIST})
    ActionType actionType;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "action_id")
    Set<ActionInputVariable> inputVars = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "action_id")
    Set<ActionResultVariable> outputVars = new HashSet<>();


    @ElementCollection
    @MapKeyColumn(name = "attr_name")
    @CollectionTable(name = "process_action_attributes")
    @Column(name = "value")
    Map<String, String> taskAttributes = new HashMap<>();

    @OneToMany(mappedBy = "sourceNode", cascade = {CascadeType.PERSIST})
    Set<FlowTransition> transitions;

    public ActionType getActionType() {
        return actionType;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public Set<FlowTransition> getTransitions() {
        return transitions;
    }

    public void setTransitions(Set<FlowTransition> transitions) {
        this.transitions = transitions;
    }

    public Set<ActionInputVariable> getInputVars() {
        return inputVars;
    }

    public void setInputVars(Set<ActionInputVariable> inputVars) {
        this.inputVars = inputVars;
    }

    public Set<ActionResultVariable> getOutputVars() {
        return outputVars;
    }

    public void setOutputVars(Set<ActionResultVariable> outputVars) {
        this.outputVars = outputVars;
    }

    public Map<String, String> getTaskAttributes() {
        return taskAttributes;
    }

    public void setTaskAttributes(Map<String, String> taskAttributes) {
        this.taskAttributes = taskAttributes;
    }
}
