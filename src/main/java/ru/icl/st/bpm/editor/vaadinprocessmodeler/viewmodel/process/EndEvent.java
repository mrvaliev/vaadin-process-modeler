package ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Resource;

public class EndEvent extends ProcessNode {

    @Override
    public Resource getIcon() {
        return VaadinIcons.CIRCLE;
    }
}
