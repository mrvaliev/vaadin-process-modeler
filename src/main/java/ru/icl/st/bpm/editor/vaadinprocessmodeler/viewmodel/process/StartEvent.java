package ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Resource;

public class StartEvent extends ProcessNode {

    public StartEvent() {
        name = "Start";
    }

    @Override
    public Resource getIcon() {
        return VaadinIcons.CIRCLE_THIN;
    }
}
