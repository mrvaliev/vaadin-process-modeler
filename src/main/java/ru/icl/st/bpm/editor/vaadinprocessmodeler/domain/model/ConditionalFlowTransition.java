package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 * Переход с условием
 */
@Entity
public class ConditionalFlowTransition extends FlowTransition {

    @NotNull
    @Column(nullable = false)
    String condition;


    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }
}
