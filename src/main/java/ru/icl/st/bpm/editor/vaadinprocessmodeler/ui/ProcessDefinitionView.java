package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui;

import com.vaadin.data.TreeData;
import com.vaadin.data.provider.DataProvider;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.ui.controller.ProcessDefinitionLogic;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process.ProcessNode;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process.StartEvent;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process.TaskNode;

import javax.annotation.PostConstruct;

@SpringComponent("procDefView")
@Scope("prototype")
public class ProcessDefinitionView extends CustomComponent {

    ProcessStepGrid stepsTable;

    @Autowired
    ProcessDefinitionLogic controller;

    DataProvider dataProvider;

    Button saveProcBtn = new Button("Сохранить процесс");

    ProcessSaveWindow processSaveWindow;

    TreeDataProvider treeDataProvider;

    @Autowired
    ProcessDefinitionTree processDefTree;

    TreeData<ProcessNode> treeData;


    @PostConstruct
    public void init() {
        treeData = new TreeData<>();

        VerticalLayout layout = new VerticalLayout();

        StartEvent processNode = new StartEvent();
        processNode.setName("Start");
        treeData.addItem(null, processNode);

        treeDataProvider = new TreeDataProvider(treeData);
        processDefTree.setDataProvider(treeDataProvider);

        layout.addComponent(processDefTree);


        processSaveWindow = new ProcessSaveWindow();

        processSaveWindow.getWindowOkBtn().addClickListener(clickEvent -> {
            processSaveWindow.close();
            UI.getCurrent().removeWindow(processSaveWindow);
//            controller.saveCurrentProcessModel(processSaveWindow.textField.getValue());
        });


        processSaveWindow.addCloseListener(closeEvent -> {
            UI.getCurrent().removeWindow(processSaveWindow);
        });

        saveProcBtn.addClickListener(clickEvent -> {
            UI.getCurrent().addWindow(processSaveWindow);
        });

        setCompositionRoot(layout);
    }


    public ProcessDefinitionTree getProcessDefTree() {
        return processDefTree;
    }


    public TreeData<ProcessNode> getTreeData() {
        return treeData;
    }

    public void setDataProvider(DataProvider dataProvider) {
        this.dataProvider = dataProvider;
        stepsTable.setDataProvider(dataProvider);
    }

    class ProcessStepGrid extends Grid<TaskNode> {
        public ProcessStepGrid() {
            addColumn(TaskNode::getName).setCaption("Имя задачи");
        }
    }
}
