package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui;

import com.vaadin.navigator.View;
import com.vaadin.spring.annotation.SpringComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

@SpringComponent
public class MainView extends MainViewDesign implements View {

    @Autowired
    ApplicationContext context;


    public void replaceForm(ProcessStepForm newForm) {
        replaceComponent(form, newForm);
        form = newForm;
    }

}
