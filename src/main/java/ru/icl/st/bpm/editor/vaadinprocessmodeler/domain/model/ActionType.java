package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model;

import javax.persistence.*;

/**
 * Тип действия для @see {@link FlowNode}
 */
@Entity
public class ActionType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    long id;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    ActionTypeEnum code = ActionTypeEnum.START_STATE;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ActionTypeEnum getCode() {
        return code;
    }

    public void setCode(ActionTypeEnum code) {
        this.code = code;
    }


    public static ActionType createWithType(ActionTypeEnum typeEnum) {
        ActionType type = new ActionType();
        type.setCode(typeEnum);
        return type;
    }
}
