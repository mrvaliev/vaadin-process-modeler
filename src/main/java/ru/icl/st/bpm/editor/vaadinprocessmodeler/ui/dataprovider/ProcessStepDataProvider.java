package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui.dataprovider;

import com.vaadin.data.provider.AbstractDataProvider;
import com.vaadin.data.provider.Query;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process.TaskNode;

import java.util.stream.Stream;

public class ProcessStepDataProvider extends AbstractDataProvider<TaskNode,String> {

    @Override
    public boolean isInMemory() {
        return false;
    }

    @Override
    public int size(Query query) {
        return 0;
    }

    @Override
    public Stream fetch(Query query) {
        return null;
    }
}
