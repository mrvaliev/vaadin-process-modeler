package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.ui.controller.ProcessDefinitionLogic;

import javax.annotation.PostConstruct;

@SpringComponent
public class TaskMenu extends CustomComponent {

    @Autowired
    ProcessDefinitionLogic controller;


    public TaskMenu() {

    }

    @PostConstruct
    public void init() {
        VerticalLayout layout = new VerticalLayout();

        Label label = new Label("Типы действий");
        setCompositionRoot(layout);
        layout.setPrimaryStyleName("tasks-list");
        Button fillingForm = new Button("Заполнение формы");
        fillingForm.setIcon(VaadinIcons.FORM);

        Button checkForm = new Button("Согласование формы");
        checkForm.setIcon(VaadinIcons.THUMBS_UP);

        Button sendEmail = new Button("Отправка уведомлений");
        sendEmail.setIcon(VaadinIcons.PAPERPLANE);
        Button processData = new Button("Обработка данных");
        processData.setIcon(VaadinIcons.FILE_PROCESS);

        Button conditionStep = new Button("Проверка условия");
        conditionStep.setIcon(VaadinIcons.PLUS_MINUS);

        layout.addComponent(label);
        layout.addComponent(fillingForm);
        layout.addComponent(checkForm);
        layout.addComponent(processData);
        layout.addComponent(sendEmail);
        layout.addComponent(conditionStep);

        fillingForm.addClickListener(event -> controller.addFillFormNode());
    }




}
