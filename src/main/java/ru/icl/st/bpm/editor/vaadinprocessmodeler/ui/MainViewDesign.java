package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui;


import com.vaadin.annotations.DesignRoot;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.InputStream;

@DesignRoot
public class MainViewDesign extends VerticalLayout {

    @Autowired
    ProcessStepForm form;

    @Autowired
    TaskMenu taskMenu;

    @Autowired
    ProcessDefinitionView processDefinitionView;

    public MainViewDesign() {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("MainViewDesign.html");
        Design.read(inputStream, this);
    }
}
