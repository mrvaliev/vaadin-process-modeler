package ru.icl.st.bpm.editor.vaadinprocessmodeler;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProcessDeploymentTest extends BaseTest {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    TaskService taskService;

    @Test
    public void testRunnedTask() {
        ProcessInstance instance =
                runtimeService.startProcessInstanceByKey("APPLICATION_APPROVING");

        List<Task> tasks = taskService.createTaskQuery().active().list();

        ProcessDefinition definition =
                repositoryService.getProcessDefinition(instance.getProcessDefinitionId());
        definition.getDeploymentId();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(instance.getProcessDefinitionId());



    }

}
