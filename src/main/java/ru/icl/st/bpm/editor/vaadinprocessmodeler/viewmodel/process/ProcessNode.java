package ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process;

import com.vaadin.server.Resource;

import java.io.Serializable;

public abstract class ProcessNode implements Serializable {

    protected String id;

    /**
     * предыдущий шаг
     */
    ProcessNode prev;

    /**
     * след. шаг процесса
     */
    ProcessNode next;

    protected String name = "Шаг процесса";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProcessNode getPrev() {
        return prev;
    }

    public void setPrev(ProcessNode prev) {
        this.prev = prev;
    }

    public ProcessNode getNext() {
        return next;
    }

    public void setNext(ProcessNode next) {
        this.next = next;
    }

    public abstract Resource getIcon();


}
