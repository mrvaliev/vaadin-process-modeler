package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model;

public enum ActionTypeEnum {

    START_STATE,
    USER_TASK,
    EXCLUSIVE_GATEWAY,
    END_STATE
}
