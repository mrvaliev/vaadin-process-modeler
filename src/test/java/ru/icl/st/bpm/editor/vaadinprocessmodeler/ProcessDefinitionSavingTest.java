package ru.icl.st.bpm.editor.vaadinprocessmodeler;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.ProcessDefinitionBuilder;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model.ActionInputVariable;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model.ActionResultVariable;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model.ActionTypeEnum;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Collections;
import java.util.UUID;

public class ProcessDefinitionSavingTest extends BaseTest {

    @Autowired
    EntityManager entityManager;

    @Autowired
    ApplicationContext applicationContext;

    @Test
    @Transactional
    public void testProcessDefinitionBuilder() {

        ProcessDefinitionBuilder builder = getNewProcessBuilder();
        builder.initNewProcessDefinition("my-process-test");
        UUID start = builder.initStartEvent();
        UUID action1 = builder.addAction("user task", ActionTypeEnum.USER_TASK);

        builder.addFlowTransition(start, action1);
        builder.addVariablesforAction(action1,
                Collections.singletonList(createInputVar("var1")),
                Collections.singletonList(createResultVariable("resultvar")));

        UUID gateway = builder.addGateway("gateway");

        builder.addFlowTransition(action1, gateway);

        UUID task1 = builder.addAction("task1", ActionTypeEnum.USER_TASK);
        UUID task2 = builder.addAction("task2", ActionTypeEnum.USER_TASK);

        builder.addFlowTransition(gateway, task1, "conditionalExpression1");
        builder.addFlowTransition(gateway, task2, "expression2");

        builder.endProcess(task2.toString());

        // save process
        builder.saveProcessDefintionToDb();
//        builder.saveProcessDefinitonToActiviti();

    }

    private ActionInputVariable createInputVar(String name) {
        ActionInputVariable inputVariable = new ActionInputVariable();
        inputVariable.setName(name);
        inputVariable.setDisplayName(name);
        return inputVariable;
    }

    private ActionResultVariable createResultVariable(String name) {
        ActionResultVariable resultVariable = new ActionResultVariable();
        resultVariable.setName(name);
        resultVariable.setDisplayName(name);
        return resultVariable;
    }

    private ProcessDefinitionBuilder getNewProcessBuilder() {
        ProcessDefinitionBuilder builder =
                applicationContext.getBean(ProcessDefinitionBuilder.class);
        return builder;
    }
}
