package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model;

import javax.persistence.*;
import java.util.UUID;


/**
 * Базовый класс для любых элементов процесса.
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class FlowElement {


    @Id
    UUID id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "process_id", nullable = false)
    ProcessDefinition process;


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FlowElement)) return false;

        FlowElement element = (FlowElement) o;

        return id.equals(element.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }


    public ProcessDefinition getProcess() {
        return process;
    }

    public void setProcess(ProcessDefinition process) {
        this.process = process;
    }
}
