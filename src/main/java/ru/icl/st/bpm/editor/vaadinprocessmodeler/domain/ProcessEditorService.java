package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain;


import org.activiti.bpmn.model.*;
import org.activiti.bpmn.model.Process;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class ProcessEditorService {

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    RuntimeService runtimeService;

    TaskService taskService;

    public void initProcessDefinition() {

    }


    public void saveProcessModel(String processName) throws IOException {

        BpmnModel bpmnModel = new BpmnModel();
        Process process = new Process();
        bpmnModel.addProcess(process);

        SequenceFlow sequenceFlow = new SequenceFlow();
        sequenceFlow.setConditionExpression("");
        ExtensionElement extensionElement = new ExtensionElement();
        ExtensionAttribute attribute = new ExtensionAttribute();
        EndEvent endEvent = createEndEvent();

        ExclusiveGateway exclusiveGateway = new ExclusiveGateway();

        String bpmnResource = processName + ".bpmn";
        Deployment deployment = repositoryService.createDeployment()
                .addBpmnModel(bpmnResource, bpmnModel)
                .name("Dynamic process deployment").deploy();

        ExclusiveGateway exclusiveGateway1;

        taskService.createTaskQuery().active().list().get(0).getOwner();

        InputStream resource = repositoryService.getResourceAsStream(deployment.getId(), bpmnResource);
        byte[] data = new byte[resource.available()];
        resource.read(data);
        FileOutputStream outputStream = new FileOutputStream("process.bpmn20.xml");
        outputStream.write(data);
        outputStream.flush();
        outputStream.close();

    }


    protected UserTask createUserTask(String id, String name, String assignee) {
        UserTask userTask = new UserTask();
        userTask.setName(name);
        userTask.setId(id);
        userTask.setAssignee(assignee);
        return userTask;
    }


    protected SequenceFlow createSequenceFlow(String from, String to) {
        SequenceFlow flow = new SequenceFlow();
        flow.setSourceRef(from);
        flow.setTargetRef(to);
        return flow;
    }

    protected StartEvent createStartEvent() {
        StartEvent startEvent = new StartEvent();
        startEvent.setId("start");
        return startEvent;
    }

    protected EndEvent createEndEvent() {
        EndEvent endEvent = new EndEvent();
        endEvent.setId("end");
        return endEvent;
    }
}
