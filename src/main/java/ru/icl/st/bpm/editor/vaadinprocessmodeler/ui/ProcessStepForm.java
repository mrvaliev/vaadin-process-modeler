package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui;

import com.vaadin.data.Binder;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.*;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process.ProcessNode;

import javax.annotation.PostConstruct;


@SpringComponent
public class ProcessStepForm<E extends ProcessNode> extends FormLayout {

    protected Binder<E> binder;
    protected E currentValue;

    TextField nameField = new TextField("Имя задачи");


    public ProcessStepForm() {
        setPrimaryStyleName("process-step-form");
        addComponent(nameField);
        binder = new Binder<>();
        binder.bind(nameField, ProcessNode::getName, ProcessNode::setName);
    }


    public void writeObject(E obj) {
        binder.readBean(obj);
        currentValue = obj;
    }


    public E getCurrentValue() {
        binder.writeBeanIfValid(currentValue);
        return currentValue;
    }
}
