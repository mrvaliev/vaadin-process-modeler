package ru.icl.st.bpm.editor.vaadinprocessmodeler;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.ProcessEditorService;

@SpringBootApplication
@ComponentScan(basePackageClasses = {VaadinProcessModelerApplication.class})
public class VaadinProcessModelerApplication {

    public static void main(String[] args) {
        SpringApplication.run(VaadinProcessModelerApplication.class, args);
    }

//    @Bean
//    public CommandLineRunner runner(ProcessEditorService service) {
//        return (String... args) -> service.saveProcessModel("test");
//    }

}
