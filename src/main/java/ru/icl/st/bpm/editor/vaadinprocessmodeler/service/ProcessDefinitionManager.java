package ru.icl.st.bpm.editor.vaadinprocessmodeler.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.ProcessDefinitionBuilder;

import javax.persistence.EntityManager;
import java.util.Map;
import java.util.UUID;

@Service
public class ProcessDefinitionManager {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    ApplicationContext applicationContext;

    private Map<String, ProcessDefinitionBuilder> processBuilders;


    public void initBusinessProcessDefinition(String name) {
        ProcessDefinitionBuilder newProcessBuilder = getNewProcessBuilder();
        newProcessBuilder.initNewProcessDefinition(name);
        processBuilders.put(newProcessBuilder.getProcessId().toString(), newProcessBuilder);
    }

    public void loadProcessDefinition(String processId) {
        UUID uuid = UUID.fromString(processId);
        ProcessDefinitionBuilder newProcessBuilder = getNewProcessBuilder();
        newProcessBuilder.loadProcessDefinition(processId);
        processBuilders.put(processId, newProcessBuilder);
    }

    public void saveProcessDefintion(String processId) {
        ProcessDefinitionBuilder processBuilder = getProcessBuilder(processId);
        processBuilder.saveProcessDefintionToDb();
        processBuilder.saveProcessDefinitonToActiviti();
    }


    public ProcessDefinitionBuilder getProcessBuilder(String processId) {
        return processBuilders.get(processId);
    }


    private ProcessDefinitionBuilder getNewProcessBuilder() {
        ProcessDefinitionBuilder builder =
                applicationContext.getBean(ProcessDefinitionBuilder.class);
        return builder;
    }


}
