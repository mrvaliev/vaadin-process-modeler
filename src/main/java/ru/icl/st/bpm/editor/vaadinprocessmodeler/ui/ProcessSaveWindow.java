package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui;


import com.vaadin.ui.*;

public class ProcessSaveWindow extends Window {

    Button windowOkBtn = new Button("OK");
//    Button windowCancelBtn = new Button("Cancel");

    TextField textField = new TextField("ProcessDefinition name");

    public ProcessSaveWindow() {
        setCaption("Сохранение процесса");
        setWidth("400px");
        setHeight("300px");
        FormLayout layout = new FormLayout();
        layout.setSizeFull();
        layout.addComponent(textField);
        layout.addComponent(windowOkBtn);
        windowOkBtn.setWidth("200px");
        setContent(layout);
        center();
    }

    public Button getWindowOkBtn() {
        return windowOkBtn;
    }
}
