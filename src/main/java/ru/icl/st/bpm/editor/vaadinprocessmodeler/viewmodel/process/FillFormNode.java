package ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process;

import com.vaadin.icons.VaadinIcons;
import com.vaadin.server.Resource;

public class FillFormNode extends ProcessNode {

    String formId;

    String assigneeUserId;

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getAssigneeUserId() {
        return assigneeUserId;
    }

    public void setAssigneeUserId(String assigneeUserId) {
        this.assigneeUserId = assigneeUserId;
    }

    @Override
    public Resource getIcon() {
        return VaadinIcons.FORM;
    }
}
