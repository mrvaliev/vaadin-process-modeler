package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model;


import javax.persistence.*;

/**
 * Результирующие даннные действия.
 */
@Entity
@DiscriminatorValue("RESULT")
public class ActionResultVariable extends ActionVariable {

}
