package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui.controller;

import com.vaadin.data.Binder;
import com.vaadin.data.provider.ListDataProvider;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.components.grid.GridSelectionModel;
import org.springframework.beans.factory.annotation.Autowired;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.ProcessEditorService;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.ui.MainView;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.ui.ProcessDefinitionView;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.ui.ProcessFillForm;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process.FillFormNode;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process.ProcessNode;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process.TaskNode;

import java.io.IOException;
import java.util.Optional;

@SpringComponent
public class ProcessDefinitionLogic {


    @Autowired
    private ProcessEditorService processEditorService;

    @Autowired
    MainView mainView;

    Binder processStepFromBinder;

    TaskNode curBean;

    ListDataProvider dataProvider;

    @Autowired
    ProcessDefinitionView processDefView;

    GridSelectionModel<TaskNode> gridSelectionModel;

    ProcessFillForm processFillForm;

    public void showFillFormEditor(FillFormNode fillFormNode) {
        ProcessFillForm form = getProcessFillForm();
        form.writeObject(fillFormNode);
        mainView.replaceForm(form);
    }


    public void addFillFormNode() {
        FillFormNode fillFormNode = new FillFormNode();
        showFillFormEditor(fillFormNode);
        addNewNode(fillFormNode);
    }


    public void addNewNode(ProcessNode newNode) {
        Optional<ProcessNode> selectedItem =
                processDefView.getProcessDefTree().getSelectionModel().getFirstSelectedItem();
        if (selectedItem.isPresent()) {
            ProcessNode node = selectedItem.get();
            node.setNext(newNode);
            newNode.setPrev(node);
            processDefView.getTreeData().addItem(null, newNode);
        }
    }


    private ProcessFillForm getProcessFillForm() {
        if (processFillForm == null) {
            processFillForm = new ProcessFillForm();
        }
        return processFillForm;
    }

}
