package ru.icl.st.bpm.editor.vaadinprocessmodeler;

import org.activiti.bpmn.model.*;
import org.activiti.bpmn.model.Process;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;


public class VaadinProcessModelerApplicationTests {

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    TaskService taskService;

    private final String nameSpace = "https://my.icl.com";
    private final String nameSpacePrefix = "pref";

    @Test
    public void contextLoads() {
    }

    @Test
    public void createAndSaveSimpleProcess() throws IOException {

        String processName = "test_process";

        BpmnModel bpmnModel = new BpmnModel();
        bpmnModel.addNamespace("activiti", "http://activiti.org/bpmn20");
        bpmnModel.addNamespace(nameSpacePrefix, nameSpace);
        Process process = new Process();
        process.setId(processName);
        process.setName(processName);
        bpmnModel.addProcess(process);
        StartEvent startEvent = createStartEvent();
        UserTask userTask1 = createUserTask("my_task_id", "my task");
        process.addFlowElement(startEvent);
        process.addFlowElement(userTask1);
        process.addFlowElement(createSequenceFlow(startEvent, userTask1));
        addTypeForTask(userTask1, "fill_form");

        ExtensionElement extensionElement = new ExtensionElement();
        extensionElement.setName("myelement");
        extensionElement.setNamespace(nameSpace);
        extensionElement.setNamespacePrefix(nameSpacePrefix);
        userTask1.addExtensionElement(extensionElement);

        SequenceFlow flow = new SequenceFlow();
        flow.setConditionExpression("value");
        process.addFlowElement(flow);


        ExtensionElement formProperty = new ExtensionElement();
        formProperty.setName("formProperty");
        formProperty.setNamespacePrefix("activiti");
        formProperty.setNamespace("http://activiti.org/bpmn20");
        formProperty.setElementText("elementText");
//        extensionElement.addChildElement(formProperty);

        extensionElement.addChildElement(formProperty);

        ExtensionAttribute attribute = new ExtensionAttribute();


        EndEvent endEvent = createEndEvent();
        process.addFlowElement(endEvent);
        process.addFlowElement(createSequenceFlow(userTask1, endEvent));

        String bpmnResource = processName + ".bpmn20";
        Deployment deployment = repositoryService.createDeployment()
                .addBpmnModel(bpmnResource, bpmnModel)
                .name("Dynamic process deployment").deploy();

        ExclusiveGateway exclusiveGateway1;

        InputStream resource = repositoryService.getResourceAsStream(deployment.getId(), bpmnResource);

        byte[] data = new byte[resource.available()];
        resource.read(data);
        FileOutputStream outputStream = new FileOutputStream(bpmnResource + ".xml");
        outputStream.write(data);
        outputStream.flush();
        outputStream.close();

    }


    private void addTypeForTask(Task task, String code) {
        ExtensionAttribute attribute = new ExtensionAttribute();
//        attribute.setNamespacePrefix(nameSpacePrefix);
        attribute.setNamespace(nameSpace);
        attribute.setName("typeCode");
        attribute.setValue(code);
        task.addAttribute(attribute);
    }


    protected UserTask createUserTask(String id, String name) {
        UserTask userTask = new UserTask();
        userTask.setName(name);
        userTask.setId(id);
        return userTask;
    }


    protected SequenceFlow createSequenceFlow(String from, String to) {
        SequenceFlow flow = new SequenceFlow();
        flow.setSourceRef(from);
        flow.setTargetRef(to);
        return flow;
    }

    protected SequenceFlow createSequenceFlow(FlowElement from, FlowElement to) {
        SequenceFlow flow = new SequenceFlow();
        flow.setSourceRef(from.getId());
        flow.setTargetRef(to.getId());
        return flow;
    }

    protected StartEvent createStartEvent() {
        StartEvent startEvent = new StartEvent();
        startEvent.setId("start");
        return startEvent;
    }

    protected EndEvent createEndEvent() {
        EndEvent endEvent = new EndEvent();
        endEvent.setId("end");
        return endEvent;
    }

}
