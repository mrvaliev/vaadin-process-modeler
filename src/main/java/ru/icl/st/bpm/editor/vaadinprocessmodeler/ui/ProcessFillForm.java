package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui;

import com.vaadin.ui.TextField;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process.FillFormNode;


public class ProcessFillForm extends ProcessStepForm<FillFormNode> {


    public ProcessFillForm() {
        TextField formIdField = new TextField("Имя формы");
        TextField userIdField = new TextField("Ответственный");
        binder.bind(formIdField, FillFormNode::getFormId, FillFormNode::setFormId);
        binder.bind(userIdField, FillFormNode::getAssigneeUserId, FillFormNode::setAssigneeUserId);
        addComponent(formIdField);
        addComponent(userIdField);
    }
}
