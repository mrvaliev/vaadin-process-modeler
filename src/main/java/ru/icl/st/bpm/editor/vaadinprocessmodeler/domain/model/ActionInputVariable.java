package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;


/**
 * Входящие данные для действия.
 */
@Entity
@DiscriminatorValue("INPUT")
public class ActionInputVariable extends ActionVariable {


}
