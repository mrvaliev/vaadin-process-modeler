package ru.icl.st.bpm.editor.vaadinprocessmodeler.ui;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.ui.Tree;
import org.springframework.beans.factory.annotation.Autowired;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.ui.controller.ProcessDefinitionLogic;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.viewmodel.process.ProcessNode;


@SpringComponent
public class ProcessDefinitionTree extends Tree<ProcessNode> {

    @Autowired
    ProcessDefinitionLogic controller;

    public ProcessDefinitionTree() {
        super("Определение процесса");
        setCaption("Определение процесса");
        setItemCaptionGenerator(el -> el.getName());
        setRowHeight(30);
        setId("process-def-tree");
        setSizeFull();
        setItemIconGenerator(processNode -> processNode.getIcon());
        setItemCaptionGenerator(processNode -> processNode.getName());
    }

}
