package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain;

import org.activiti.bpmn.model.*;
import org.activiti.bpmn.model.Process;
import org.activiti.engine.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model.*;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model.FlowElement;
import ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model.FlowNode;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.*;


@Component("processDefBuilder")
@Scope("prototype")
public class ProcessDefinitionBuilder {

    private ProcessDefinition process;

    private Process bpmnProcess = new Process();

    private Map<UUID, FlowElement> elements = new LinkedHashMap<>();

    private BpmnModel bpmnModel = new BpmnModel();

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private RepositoryService repositoryService;

    private UUID getUUID() {
        return UUID.randomUUID();
    }

    public void initNewProcessDefinition(String name) {
        process = new ProcessDefinition();
        process.setId(getUUID());
        process.setName(name);
        bpmnProcess.setName(name);
        bpmnProcess.setId(name);
        bpmnModel.addProcess(bpmnProcess);
    }

    public UUID initStartEvent() {
        FlowNode start = new FlowNode();
        start.setActionType(ActionType.createWithType(ActionTypeEnum.START_STATE));
        start.setTitle("Start");
        start.setId(getUUID());
        process.setStartState(start);
        process.addElement(start);
        elements.put(start.getId(), start);
        ServiceTask serviceTask=new ServiceTask();
        serviceTask.setCustomProperties();
        // activiti definition
        StartEvent startEvent = new StartEvent();
        startEvent.setId(start.getId().toString());
        bpmnProcess.addFlowElement(startEvent);
        return start.getId();
    }

    public UUID addAction(String title, ActionTypeEnum actionType) {
        FlowNode action = new FlowNode();
        action.setTitle(title);
        action.setActionType(ActionType.createWithType(actionType));
        action.setId(getUUID());
        elements.put(action.getId(), action);
        process.addElement(action);
        UserTask task = new UserTask();
        task.setName(title);
        task.setId(action.getId().toString());
        bpmnProcess.addFlowElement(task);
        // something else ...
        return action.getId();
    }


    public void addVariablesforAction(UUID id, List<ActionInputVariable> inputVariables,
                                      List<ActionResultVariable> resultVariables) {

        FlowNode action = (FlowNode) elements.get(id);
        if (action != null) {
            if (inputVariables != null) {
                action.getInputVars().addAll(inputVariables);
            }
            if (resultVariables != null) {
                action.getOutputVars().addAll(resultVariables);
            }
        }
    }

    public UUID addGateway(String title) {
        FlowNode gateway = new FlowNode();
        gateway.setTitle(title);
        gateway.setActionType(ActionType.createWithType(ActionTypeEnum.EXCLUSIVE_GATEWAY));
        gateway.setId(getUUID());
        elements.put(gateway.getId(), gateway);
        return gateway.getId();
    }

    public void deleteElement(UUID uuid) {
        process.removeElement(uuid);
        bpmnProcess.removeFlowElement(uuid.toString());
        if (elements.containsKey(uuid)) {
            elements.remove(uuid);
        }
    }

    public UUID addFlowTransition(UUID sourceElemId, UUID targetElemId,
                                  String conditionalExpression) {

        FlowTransition transition;

        if (conditionalExpression == null || conditionalExpression.trim().isEmpty()) {
            transition = new FlowTransition();
        } else {
            transition = new ConditionalFlowTransition();
            ((ConditionalFlowTransition) transition).setCondition(conditionalExpression);
        }
        transition.setId(getUUID());
        elements.put(transition.getId(), transition);
        transition.setSourceNode((FlowNode) elements.get(sourceElemId));
        transition.setTargetNode((FlowNode) elements.get(targetElemId));
        SequenceFlow sequenceFlow = new SequenceFlow();
        sequenceFlow.setSourceRef(sourceElemId.toString());
        sequenceFlow.setTargetRef(targetElemId.toString());
        sequenceFlow.setId(transition.getId().toString());
        sequenceFlow.setConditionExpression(conditionalExpression);
        return transition.getId();
    }

    public UUID addFlowTransition(UUID sourceElemId, UUID targetElemId) {
        return addFlowTransition(sourceElemId, targetElemId, null);
    }

    public void loadProcessDefinition(String processId) {
        bpmnModel = repositoryService.getBpmnModel(processId);
        bpmnProcess = bpmnModel.getProcessById(processId);

        // load our model database
        elements.clear();
        UUID uuid = UUID.fromString(processId);
        TypedQuery<ProcessDefinition> query =
                entityManager.createQuery("SELECT p FROM ProcessDefinition p JOIN FETCH p.elements WHERE p.id=:id",
                        ProcessDefinition.class);
        query.setParameter("id", uuid);
        ProcessDefinition processDefinition = query.getSingleResult();
        Set<FlowElement> flowNodes = processDefinition.getElements();
        for (FlowElement element : flowNodes) {
            elements.put(element.getId(), element);
        }
    }


    public void endProcess(String fromId) {
        EndEvent endEvent = new EndEvent();
        bpmnProcess.addFlowElement(endEvent);
        SequenceFlow sequenceFlow = new SequenceFlow();
        sequenceFlow.setTargetFlowElement(endEvent);
        sequenceFlow.setSourceRef(fromId);
    }

    public void saveProcessDefintionToDb() {
        for (FlowElement element : elements.values()) {
            process.addElement(element);
        }
        entityManager.persist(process);
    }

    public void saveProcessDefinitonToActiviti() {
        repositoryService.createDeployment()
                .name("process-definition-builder")
                .addBpmnModel(process.getName() + "bpmn20.xml", bpmnModel).deploy();
    }

    public UUID getProcessId() {
        return process.getId();
    }

    public UUID getStartElementId() {
        return process.getStartState().getId();
    }


    public ProcessDefinition getProcess() {
        return process;
    }

    public Process getBpmnProcess() {
        return bpmnProcess;
    }


    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
