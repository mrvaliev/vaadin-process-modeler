package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "process_definition")
public class ProcessDefinition {

    @Id
    UUID id;

    String name;

    String createdBy;

    @OneToOne
    @JoinColumn(name = "start_action")
    FlowNode startState;


    @OneToMany(mappedBy = "process", cascade = {CascadeType.ALL})
    Set<FlowElement> elements = new LinkedHashSet<>();

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public FlowNode getStartState() {
        return startState;
    }

    public void setStartState(FlowNode startState) {
        startState.setProcess(this);
        this.startState = startState;
        elements.add(startState);
    }

    public void addElement(FlowElement element) {
        element.setProcess(this);
        elements.add(element);
    }


    public Set<FlowElement> getElements() {
        return elements;
    }

    public void removeElement(UUID uuid) {
        FlowNode flowNode = new FlowNode();
        flowNode.setId(uuid);
        elements.remove(flowNode);
    }


}
