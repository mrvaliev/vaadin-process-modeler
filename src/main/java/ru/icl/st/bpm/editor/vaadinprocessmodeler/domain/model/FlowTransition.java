package ru.icl.st.bpm.editor.vaadinprocessmodeler.domain.model;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Переход (стрелка)
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class FlowTransition extends FlowElement {


    @NotNull
    @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "source_node")
    FlowNode sourceNode;

    @NotNull
    @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "target_node")
    FlowNode targetNode;


    public FlowNode getSourceNode() {
        return sourceNode;
    }

    public void setSourceNode(FlowNode sourceNode) {
        this.sourceNode = sourceNode;
    }

    public FlowNode getTargetNode() {
        return targetNode;
    }

    public void setTargetNode(FlowNode targetNode) {
        this.targetNode = targetNode;
    }

}
